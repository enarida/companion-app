package batstate.commutersalertbutton

import android.graphics.Bitmap
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebChromeClient
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import kotlinx.android.synthetic.main.fragment_mmda.*


class MMDAFragment : Fragment() {

    private val url = "https://www.mayhuliba.com/index.php"

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        return inflater!!.inflate(R.layout.fragment_mmda, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        //setup webView Client
        webView.webChromeClient = WebChromeClient()
        webView.webViewClient = MyBrowser()

        val settings = webView.settings
        settings.javaScriptEnabled = true
        settings.javaScriptCanOpenWindowsAutomatically = true
        settings.domStorageEnabled = true

        webView.loadUrl(url)
    }

    private inner class MyBrowser : WebViewClient() {

        override fun onPageFinished(view: WebView, url: String) {
            if(isAdded) {
                progressBar?.visibility = View.GONE
                webView.setOnTouchListener({ _, _ -> false })
            }
        }

        override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
            super.onPageStarted(view, url, favicon)

            webView.setOnTouchListener({ _, _ -> true }) // disable touch on webview
        }

        override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {
            view?.loadUrl(url)
            return super.shouldOverrideUrlLoading(view, request)
        }
    }
}
