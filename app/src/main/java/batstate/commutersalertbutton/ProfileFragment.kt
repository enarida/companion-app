package batstate.commutersalertbutton

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ListView
import batstate.commutersalertbutton.adapter.ProfileAdapter
import batstate.commutersalertbutton.util.NonScrollListView
import batstate.commutersalertbutton.util.SettingsUtil

class ProfileFragment : Fragment() {

    private var mProfileAdapter: ProfileAdapter? = null

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater!!.inflate(R.layout.fragment_profile, container, false)

        // Setup Profile ListView
        mProfileAdapter = ProfileAdapter(context)
        mProfileAdapter!!.addItem(
                SettingsUtil.KEY_FIRST_NAME,
                getString(R.string.first_name),
                SettingsUtil.getFirstName(context),
                ProfileAdapter.FILTER_NAME)
        mProfileAdapter!!.addItem(
                SettingsUtil.KEY_LAST_NAME,
                getString(R.string.last_name),
                SettingsUtil.getLastName(context),
                ProfileAdapter.FILTER_NAME)

        val profileList = view.findViewById<NonScrollListView>(R.id.list_view_profile) as ListView
        profileList.adapter = mProfileAdapter
        profileList.onItemClickListener = mProfileAdapter

        return view
    }

    fun getAdapter(): ProfileAdapter? {
        return mProfileAdapter
    }
}
