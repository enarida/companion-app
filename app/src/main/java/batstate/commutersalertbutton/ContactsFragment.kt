package batstate.commutersalertbutton

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.provider.ContactsContract
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ListView
import android.widget.Toast
import batstate.commutersalertbutton.adapter.ContactAdapter
import batstate.commutersalertbutton.data.ContactData
import batstate.commutersalertbutton.util.NonScrollListView
import batstate.commutersalertbutton.util.SettingsUtil

class ContactsFragment : Fragment() {

    private val requestSelectedContact = 1
    private var mContactAdapter: ContactAdapter? = null

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater!!.inflate(R.layout.fragment_contacts, container, false)

        val contactList = view?.findViewById<NonScrollListView>(R.id.list_view_contacts) as ListView
        mContactAdapter = ContactAdapter(context, true)

//      Button event for contacts
        val btn_addContacts = view.findViewById<Button>(R.id.btn_addContacts) as Button
        btn_addContacts.setOnClickListener {
            val intent = Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI)
            startActivityForResult(intent, requestSelectedContact)
        }

//      Add of contacts
        for (contact in SettingsUtil.getContacts(context))
            mContactAdapter!!.addItem(contact)
        mContactAdapter!!.setPriorityContact(SettingsUtil.getPriorityContactIndex(context))

        contactList.adapter = mContactAdapter
        contactList.onItemClickListener = mContactAdapter

        return view
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {


//       Get the contact details
        if (resultCode == Activity.RESULT_OK) {

            val cursor = context.contentResolver.query(data!!.data, null, null, null, null)!!

            if (cursor.moveToFirst()) {

                val nameIndex = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME))
                var phoneIndex = ""
                val idIndex = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID))
                var hasPhone = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))

                hasPhone = if (hasPhone.equals("1", ignoreCase = true))
                    "true"
                else
                    "false"

                if (java.lang.Boolean.parseBoolean(hasPhone)) {
                    val phones = context.contentResolver.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + idIndex, null, null)
                    while (phones!!.moveToNext()) {
                        phoneIndex = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER))
                    }
                    phones.close()
                }

                val contact = ContactData()
                contact.name = nameIndex
                contact.contactNo = phoneIndex

                if (!mContactAdapter?.contains(contact)!!) {
                    mContactAdapter?.addItem(contact)

                    if (mContactAdapter!!.count == 1) {
                        mContactAdapter!!.setPriorityContact(0)
                    }
                } else
                    Toast.makeText(context, "Contact already in list", Toast.LENGTH_SHORT).show()
            } else run { Toast.makeText(context, "Invalid Contact", Toast.LENGTH_SHORT).show() }
        }
    }

    fun getAdapter(): ContactAdapter? {
        return mContactAdapter
    }
}
