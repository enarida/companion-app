package batstate.commutersalertbutton.util;

public interface OnStatusChangeListener {
    enum Status {
        NO_ERROR, HAS_ERROR
    }

    void onStatusChange(Status status);
}
