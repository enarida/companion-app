package batstate.commutersalertbutton.util;

import android.content.Context;
import android.content.SharedPreferences;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import batstate.commutersalertbutton.data.ContactData;

@SuppressWarnings("WeakerAccess")
public class SettingsUtil {

    public static final String PREFS_NAME = "EPB_Settings";
    public static final String KEY_CONFIGURED = "configured";
    public static final String KEY_FIRST_NAME = "firstName";
    public static final String KEY_LAST_NAME = "lastName";
    public static final String KEY_CONTACTS = "contacts";
    public static final String KEY_PRIORITY_CONTACT_INDEX = "priorityContactIndex";
    public static final String KEY_PLATE_NUMBER = "plateNumber";

    private SettingsUtil() {
    }

    public static SharedPreferences getSharedPreferences(Context context) {
        return context.getSharedPreferences(PREFS_NAME, 0);
    }

    public static String getFirstName(Context context) {
        return getSharedPreferences(context).getString(KEY_FIRST_NAME, "");
    }

    public static String getLastName(Context context) {
        return getSharedPreferences(context).getString(KEY_LAST_NAME, "");
    }

    public static String getFullName(Context context) {
        return getSharedPreferences(context).getString(KEY_FIRST_NAME, "") + " " + getSharedPreferences(context).getString(KEY_LAST_NAME, "");
    }

    public static String getKeyPlateNumber(Context context) {
        return getSharedPreferences(context).getString(KEY_PLATE_NUMBER, "");
    }

    public static ArrayList<ContactData> getContacts(Context context) {
        ArrayList<ContactData> contacts = new ArrayList<>();

        try {
            JSONArray contactsArray = new JSONArray(getSharedPreferences(context).getString(KEY_CONTACTS, "[]"));
            contacts.ensureCapacity(contactsArray.length());

            for (int i = 0; i < contactsArray.length(); i++) {
                JSONObject entry = contactsArray.getJSONObject(i);
                ContactData contact = new ContactData();
                contact.name = entry.getString("name");
                contact.contactNo = entry.getString("contactNo");
                contacts.add(contact);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return contacts;
    }

    public static ContactData getPriorityContact(Context context) {
        ArrayList<ContactData> contacts = getContacts(context);
        int index = getSharedPreferences(context).getInt(KEY_PRIORITY_CONTACT_INDEX, -1);

        if (contacts != null && index >= 0)
            return contacts.get(index);
        return null;
    }

    public static int getPriorityContactIndex(Context context) {
        return getSharedPreferences(context).getInt(KEY_PRIORITY_CONTACT_INDEX, 0);
    }

    public static boolean isConfigured(Context context) {
        return getSharedPreferences(context).getBoolean(KEY_CONFIGURED, false);
    }

    public static void setString(Context context, String key, String value) {
        getSharedPreferences(context).edit().putString(key, value).apply();
    }

    public static void setInt(Context context, String key, int value) {
        getSharedPreferences(context).edit().putInt(key, value).apply();
    }

    public static void setConfigured(Context context) {
        getSharedPreferences(context).edit().putBoolean(SettingsUtil.KEY_CONFIGURED, true).apply();
    }
}
