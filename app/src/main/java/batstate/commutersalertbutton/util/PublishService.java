package batstate.commutersalertbutton.util;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.provider.Settings;
import android.telephony.SmsManager;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;

import batstate.commutersalertbutton.R;
import batstate.commutersalertbutton.data.ContactData;

public class PublishService extends IntentService {

    private static final String TAG = "PublishService";

    private LocationFinder mLocationFinder;
    private SmsManager mSmsManager;

    public PublishService() {
        super("PublishService");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mLocationFinder = new LocationFinder(this);
        mLocationFinder.start();
        mSmsManager = SmsManager.getDefault();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mLocationFinder.isStarted())
            mLocationFinder.stop();
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        // Wait till we have a locationFix fix
        Location locationFix = null;
        if (mLocationFinder.canGetLocation())
            locationFix = mLocationFinder.waitLocation();

        // Check internet connectivity
        ConnectivityManager connectivityManager =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = null;
        if (connectivityManager != null) {
            activeNetwork = connectivityManager.getActiveNetworkInfo();
        }
        boolean isConnected = activeNetwork != null && activeNetwork.isConnected();

        // Construct Message
        Log.d(TAG, "Constructing Message...");
        String header = getString(R.string.panic_message_header);
        String profile = getString(R.string.panic_message_profile,
                SettingsUtil.getFullName(this),
                SettingsUtil.getKeyPlateNumber(this));
        String mapLabel = getString(R.string.panic_message_map) + " ";
        String phoneModel = getString(R.string.panic_message_device, Build.MODEL);

        String locationStr = "", mapLink;
        if (locationFix != null) {
            String address = "";

            if (isConnected)
                address = LocationFinder.getAddress(this, locationFix);

            locationStr = getString(R.string.panic_message_location,
                    address,
                    locationFix.getLatitude(),
                    locationFix.getLongitude());

            mapLink = getString(R.string.panic_message_map_link,
                    locationFix.getLatitude(),
                    locationFix.getLongitude());
        } else {
            Toast.makeText(this, getString(R.string.panic_message_location_na), Toast.LENGTH_SHORT).show();
            mapLink = getString(R.string.panic_message_map_link, 0f, 0f);
        }

        // Construct SMS Message
        StringBuilder smsMessage = new StringBuilder(header);
        smsMessage.append("\n").append(profile);

        if (locationFix != null) {
            smsMessage.append("\n").append(locationStr);
            smsMessage.append("\n").append(mapLabel).append(mapLink);
        } else {
            smsMessage.append("\n").append(getString(R.string.panic_message_location_na));
        }

        smsMessage.append("\n").append(phoneModel);
        Log.d(TAG, "**********Panic Message**********\n" + smsMessage.toString());

        // Send Message through SMS
        ArrayList<String> dividedMsg = mSmsManager.divideMessage(smsMessage.toString());

        try {
            for (ContactData contact : SettingsUtil.getContacts(this)) {
                Log.d(TAG, "Sending message to " + contact.name);
                mSmsManager.sendMultipartTextMessage(contact.contactNo, null, dividedMsg, null, null);
            }
        } catch (Exception e) {
            Toast.makeText(this, "Unable to send. Please check your settings if there is valid data.", Toast.LENGTH_LONG).show();
        }
    }
}
