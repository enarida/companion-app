package batstate.commutersalertbutton.util;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class LocationFinder {

    private static final String TAG = "LocationFinder";
    private static final int LOCATION_WAIT_TIMEOUT = 1000 * 60; // 1 minute
    private static final int LOCATION_EXPIRE_TIME = 1000 * 30; // 30 seconds
    private static final int LOCATION_INTERVAL = 1000 * 10; // 10 seconds
    private static final int LOCATION_MIN_DISTANCE = 50; // 50 meters

    private final Context mContext;
    private final LocationManager mLocationManager;
    private final LocationListener mLocationListener;
    private boolean mCanGetLocation, mIsStarted;
    private Location mLocation;

    LocationFinder(Context context) {
        mContext = context;
        mLocationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        mLocationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                if (isBetterLocation(location, mLocation)) {
                    mLocation = location;
                    Log.d(TAG, "New Best Location Received!\n" + mLocation.toString());
                }
            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {

            }

            @Override
            public void onProviderEnabled(String s) {

            }

            @Override
            public void onProviderDisabled(String s) {

            }
        };

    }

    public void start() {
        if (mIsStarted) {
            Log.e(TAG, "Starting location finder while it's already started");
            return;
        }

        // Check if we have a location provider
        boolean isNetworkEnabled = mLocationManager.isProviderEnabled(android.location.LocationManager.NETWORK_PROVIDER);
        boolean isGpsEnabled = mLocationManager.isProviderEnabled(android.location.LocationManager.GPS_PROVIDER);

        if (!isNetworkEnabled && !isGpsEnabled) {
            mCanGetLocation = false;
        } else if (ContextCompat.checkSelfPermission(mContext,
                Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            mCanGetLocation = true;
            mIsStarted = true;

            if (isNetworkEnabled) {
                Log.d(TAG, "Network Provider is enabled");
                mLocationManager.requestLocationUpdates(
                        android.location.LocationManager.NETWORK_PROVIDER,
                        LOCATION_INTERVAL,
                        LOCATION_MIN_DISTANCE,
                        mLocationListener);
            }

            if (isGpsEnabled) {
                Log.d(TAG, "GPS Provider is enabled");
                mLocationManager.requestLocationUpdates(
                        android.location.LocationManager.GPS_PROVIDER,
                        LOCATION_INTERVAL,
                        LOCATION_MIN_DISTANCE,
                        mLocationListener);
            }
        }
    }

    void stop() {
        if (!mIsStarted) {
            Log.e(TAG, "Stopping location finder while it's stopped");
            return;
        }

        if (ContextCompat.checkSelfPermission(mContext,
                Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            mLocationManager.removeUpdates(mLocationListener);
        }
    }

    public boolean isStarted(){
        return mIsStarted;
    }

    boolean canGetLocation() {
        return mCanGetLocation;
    }

    Location getLocation() {
        return mLocation;
    }

    Location waitLocation() {
        long startTime = System.currentTimeMillis();
        while ((System.currentTimeMillis() - startTime) < LOCATION_WAIT_TIMEOUT) {
            if (mLocation != null)
                break;
            try {
                Log.d(TAG, "Waiting for location fix...");
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        return getLocation();
    }

    static String getAddress(Context context, Location location) {
        return getAddress(context, location.getLatitude(), location.getLongitude());
    }

    private static String getAddress(Context context, double latitude, double longitude) {
        // Get Address from location fix
        Geocoder geocoder = new Geocoder(context, Locale.getDefault());
        StringBuilder address = new StringBuilder();
        try {
            List<Address> addresses = geocoder.getFromLocation(latitude, longitude, 1);

//            TODO ERROR java.lang.IndexOutOfBoundsException: Invalid index 0, size is 0
        for (int i = 0; i <= addresses.get(0).getMaxAddressLineIndex(); i++)
                address.append(addresses.get(0).getAddressLine(i)).append(", ");
            return address.substring(0, address.length() - 2);
        } catch (IOException e) {
            e.printStackTrace();
            return "N/A";
        }
    }

    private boolean isBetterLocation(Location newLoc, Location lastLoc) {
        // New location is better because we doesn't have a location to compare with
        if (lastLoc == null)
            return true;

        // Check if the last location is already expired
        long timeDelta = newLoc.getTime() - lastLoc.getTime();
        boolean isSignificantlyNewer = timeDelta > LOCATION_EXPIRE_TIME;
        boolean isSignificantlyOlder = timeDelta < -LOCATION_EXPIRE_TIME;
        boolean isNewer = timeDelta > 0;

        if (isSignificantlyNewer)
            return true;
        else if (isSignificantlyOlder)
            return false;

        // Check if the new location is more or less accurate
        int accuracyDelta = (int) (newLoc.getAccuracy() - lastLoc.getAccuracy());
        boolean isLessAccurate = accuracyDelta > 0;
        boolean isMoreAccurate = accuracyDelta < 0;
        boolean isSignificantlyLessAccurate = accuracyDelta > 200;
        boolean isFromSameProvider = isSameProvider(newLoc.getProvider(), lastLoc.getProvider());

        if (isMoreAccurate)
            return true;
        else if (isNewer && !isLessAccurate)
            return true;
        else if (isNewer && !isSignificantlyLessAccurate && isFromSameProvider)
            return true;

        return false;
    }

    private boolean isSameProvider(String provider1, String provider2) {
        if (provider1 == null)
            return provider2 == null;

        return provider1.equals(provider2);
    }

}
