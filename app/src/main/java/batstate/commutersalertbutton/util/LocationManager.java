package batstate.commutersalertbutton.util;

import android.Manifest;
import android.app.Activity;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;

import static android.app.Activity.RESULT_OK;

public class LocationManager {

    private static final String TAG = LocationManager.class.getSimpleName();
    private static final int REQUEST_LOCATION_PERMISSION = 3234;
    private static final int REQUEST_CHECK_SETTINGS = 8768;
    private static final int RESOLVE_CONNECTION = 4784;

    private final Activity activity;
    private final GoogleApiClient googleApiClient;
    private Location currentLocation;
    private LocationListener locationListener;

    private boolean checkForLocationSettings;

    public LocationManager(Activity activity) {
        this(activity, true);
    }

    public LocationManager(final Activity activity, boolean checkForLocationSettings) {
        this.activity = activity;
        this.checkForLocationSettings = checkForLocationSettings;

        googleApiClient = new GoogleApiClient.Builder(activity)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                    @Override
                    public void onConnected(@Nullable Bundle bundle) {
                        listenForLocations();
                    }

                    @Override
                    public void onConnectionSuspended(int i) {
                        new AlertDialog.Builder(activity)
                                .setTitle("Error")
                                .setMessage("Something happened to Google Play Services...")
                                .setPositiveButton(android.R.string.ok, null)
                                .show();
                    }
                })
                .addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
                        try {
                            connectionResult.startResolutionForResult(activity, RESOLVE_CONNECTION);
                        } catch (IntentSender.SendIntentException e) {
                            e.printStackTrace();
                        }
                    }
                })
                .build();
    }

    public void setLocationListener(LocationListener locationListener) {
        this.locationListener = locationListener;
    }

    public Location getCurrentLocation() {
        return currentLocation;
    }

    public void setCurrentLocation(Location currentLocation) {
        this.currentLocation = currentLocation;
    }

    public void start() {
        if (!googleApiClient.isConnected())
            googleApiClient.connect();
    }

    public void stop() {
        if (googleApiClient.isConnected())
            googleApiClient.disconnect();
    }

    public void onActivityResult(int requestCode, int resultCode) {
        if (requestCode == REQUEST_CHECK_SETTINGS && resultCode == RESULT_OK)
            listenForLocations();
        else if (requestCode == RESOLVE_CONNECTION && resultCode == RESULT_OK)
            googleApiClient.connect();
    }

    private void listenForLocations() {
        final LocationRequest locationRequest = new LocationRequest()
                .setInterval(5000)
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        final LocationListener locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                Log.d(TAG, location.toString());

                // Check the location age
//                Log.d(TAG, "Current System Time: " + System.currentTimeMillis() + "ms");
//                Log.d(TAG, "Location Fix Time: " + location.getTime() + "ms");
//                long delta = System.currentTimeMillis() - location.getTime();
//                Log.d(TAG, "Delta: " + delta + "ms");
//                if (delta > 5 * 60 * 1000) {
//                    Log.d(TAG, "Location is too old!");
//                    return;
//                }

                // Check the location's accuracy
                if (location.getAccuracy() >= 80) {
                    Log.d(TAG, "Location is too inaccurate!");
                    return;
                }

                currentLocation = location;

                // Call listeners
                if (LocationManager.this.locationListener != null)
                    LocationManager.this.locationListener.onLocationChanged(currentLocation);
            }
        };

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);
        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(@NonNull LocationSettingsResult result) {
                final Status status = result.getStatus();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        int currentPermission = ActivityCompat.checkSelfPermission(
                                activity, Manifest.permission.ACCESS_FINE_LOCATION);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M &&
                                currentPermission != PackageManager.PERMISSION_GRANTED) {
                            String[] neededPermissions = new String[]{Manifest.permission.ACCESS_FINE_LOCATION};
                            activity.requestPermissions(neededPermissions, REQUEST_LOCATION_PERMISSION);
                            return;
                        }

                        // Start Tracking
                        LocationServices.FusedLocationApi.requestLocationUpdates(
                                googleApiClient, locationRequest, locationListener);
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        if (!checkForLocationSettings) {
                            return;
                        }

                        checkForLocationSettings = false;

                        try {
                            status.startResolutionForResult(activity, REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {
                            e.printStackTrace();
                        }
                        break;
                }
            }
        });
    }
}
