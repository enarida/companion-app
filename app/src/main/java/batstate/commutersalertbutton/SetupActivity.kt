package batstate.commutersalertbutton

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import batstate.commutersalertbutton.util.OnStatusChangeListener
import batstate.commutersalertbutton.util.SettingsUtil
import kotlinx.android.synthetic.main.activity_setup.*

class SetupActivity : AppCompatActivity() {

    private val FRAGMENT_TAG = "CURRENT_FRAGMENT"

    private var mProfile: ProfileFragment? = null
    private var mContacts: ContactsFragment? = null

    private var mCurrentFragment: Int = 0
    private var mStatusListener: OnStatusChangeListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_setup)

        mProfile = ProfileFragment()
        mContacts = ContactsFragment()

        mStatusListener = OnStatusChangeListener { status ->
            btn_next!!.isEnabled = status === OnStatusChangeListener.Status.NO_ERROR
        }

        onControlClicked(window.decorView)
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)
        assert(supportActionBar != null)
        supportActionBar!!.setDisplayShowHomeEnabled(true)

        mCurrentFragment = 0
        updateContent()
    }

    override fun onBackPressed() {
        if (mCurrentFragment == 0)
            super.onBackPressed()
        else
            btn_previous.performClick()
    }

//    Setup buttons clicked
    fun onControlClicked(v: View) {
        if (v.id == R.id.btn_previous) {
            mCurrentFragment--

            if (mCurrentFragment == 0)
                btn_previous!!.isEnabled = false
        } else if (v.id == R.id.btn_next) {
            mCurrentFragment++

            if (mCurrentFragment > 0)
                btn_previous!!.isEnabled = true
        }

        when (mCurrentFragment) {
            0 -> btn_next!!.setText(R.string.next)
            1 -> btn_next!!.setText(R.string.finish)
        }

        updateContent()
    }

//    Updates fragment
    private fun updateContent() {
        val actionBar = supportActionBar!!
        val fm = supportFragmentManager
        val ft = fm.beginTransaction()
        when (mCurrentFragment) {
            0 -> {
                actionBar.title = "Setup Profile"
                ft.replace(R.id.content_frame, mProfile, FRAGMENT_TAG)
            }
            1 -> {
                actionBar.title = "Setup Contacts"
                ft.replace(R.id.content_frame, mContacts, FRAGMENT_TAG)
            }
            2 -> {
                SettingsUtil.setConfigured(this)
                val intent = Intent(this, NavigationActivity::class.java)
                startActivity(intent)
                finish()
            }
        }
        ft.commitNow()
        supportFragmentManager.executePendingTransactions()

        // On Fragment Created
        when (mCurrentFragment) {
            0 -> {
                if(mProfile!!.getAdapter() != null) {
                    mProfile!!.getAdapter()!!.setOnStatusChangeListener(mStatusListener)
                    if (mProfile!!.getAdapter()!!.hasErrors())
                        btn_next!!.isEnabled = false
                }

            }
            1 -> {
                if(mContacts!!.getAdapter() != null) {
                    mContacts!!.getAdapter()!!.setOnStatusChangeListener(mStatusListener)
                    if (mContacts!!.getAdapter()!!.count == 0)
                        btn_next!!.isEnabled = false
                }
            }
            else -> {
                if (mProfile!!.getAdapter() != null)
                    mProfile!!.getAdapter()!!.setOnStatusChangeListener(null)
                if (mContacts!!.getAdapter() != null)
                    mContacts!!.getAdapter()!!.setOnStatusChangeListener(null)
            }
        }
    }
}
