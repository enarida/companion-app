package batstate.commutersalertbutton

import android.Manifest
import android.annotation.TargetApi
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.Toast
import batstate.commutersalertbutton.util.SettingsUtil

class LauncherActivity : AppCompatActivity() {

    private val REQUEST_PERMISSION = 1
    private val TAG = "LauncherActivity"

    public override fun onResume() {
        super.onResume()

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            setupPermissions()
            return
        }

        proceed()
    }

    private fun proceed() {
        val isConfigured = SettingsUtil.isConfigured(this)
        // Check if the settings is already configured
        val intent: Intent
        intent = if (isConfigured) {
            Intent(this, NavigationActivity::class.java)
        } else {
            Intent(this, SetupActivity::class.java)
        }

        startActivity(intent)
        finish()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        if (requestCode == REQUEST_PERMISSION) {
            var shouldFinish = false
            for (i in permissions.indices) {
                val isGranted = grantResults[i] == PackageManager.PERMISSION_GRANTED
                if (!isGranted) {
                    try {
                        // Get permission label
                        val packageManager = packageManager
                        val permissionInfo = packageManager.getPermissionInfo(permissions[i], 0)
                        val label = permissionInfo.loadLabel(packageManager).toString()
                        val prompt = String.format(getString(R.string.permission_not_granted), label)

                        // Show Toast
                        Log.d(TAG, prompt)
                        Toast.makeText(this, prompt, Toast.LENGTH_SHORT).show()
                    } catch (e: PackageManager.NameNotFoundException) {
                        e.printStackTrace()
                    }

                    shouldFinish = true
                }
            }

            if (shouldFinish)
                finish()
            else
                proceed()
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    private fun setupPermissions() {
        val permissions = arrayListOf<String>()

        if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_DENIED)
            permissions.add(Manifest.permission.ACCESS_FINE_LOCATION)
        if (checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_DENIED)
            permissions.add(Manifest.permission.ACCESS_COARSE_LOCATION)
        if (checkSelfPermission(Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_DENIED)
            permissions.add(Manifest.permission.READ_CONTACTS)
        if (checkSelfPermission(Manifest.permission.SEND_SMS) == PackageManager.PERMISSION_DENIED)
            permissions.add(Manifest.permission.SEND_SMS)
        if (checkSelfPermission(Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_DENIED)
            permissions.add(Manifest.permission.READ_PHONE_STATE)
        if (checkSelfPermission(Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_DENIED)
            permissions.add(Manifest.permission.CALL_PHONE)
        if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED)
            permissions.add(Manifest.permission.WRITE_EXTERNAL_STORAGE)

        if (permissions.size > 0)
            requestPermissions(permissions.toArray(arrayOfNulls<String>(0)), REQUEST_PERMISSION)
        else
            proceed()
    }
}