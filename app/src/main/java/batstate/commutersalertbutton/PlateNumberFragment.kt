package batstate.commutersalertbutton

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ListView
import batstate.commutersalertbutton.adapter.PlateNumberAdapter
import batstate.commutersalertbutton.util.NonScrollListView
import batstate.commutersalertbutton.util.SettingsUtil

class PlateNumberFragment : Fragment() {

    private var mHomeAdapter: PlateNumberAdapter? = null

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val view = inflater!!.inflate(R.layout.fragment_plate_number, container, false)

//      Setup Platenumber listView
        mHomeAdapter = PlateNumberAdapter(context)
        mHomeAdapter!!.addItem(
                SettingsUtil.KEY_PLATE_NUMBER,
                getString(R.string.plate_number),
                SettingsUtil.getKeyPlateNumber(context),
                PlateNumberAdapter.FILTER_PLATE_NUMBER)

        val profileList = view!!.findViewById<NonScrollListView>(R.id.list_view_plateNumber) as ListView
        profileList.adapter = mHomeAdapter
        profileList.onItemClickListener = mHomeAdapter

        return view
    }

//    TODO Ask if plate number is needed when alert button
    fun getAdapter(): PlateNumberAdapter? {
        return mHomeAdapter
    }
}