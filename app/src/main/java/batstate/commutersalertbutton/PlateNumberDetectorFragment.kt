package batstate.commutersalertbutton

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.telephony.SmsManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import kotlinx.android.synthetic.main.fragment_plate_number_detector.*


class PlateNumberDetectorFragment : Fragment() {

    private var smsManager: SmsManager? = null

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        smsManager = SmsManager.getDefault()

        return inflater!!.inflate(R.layout.fragment_plate_number_detector, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupButton()
    }

//    Sends SMS to LTO number
    private fun setupButton() {
        btn_searchPlateNumber.setOnClickListener {
            val number = "2600"
            val platenumber = txt_plateNumber.text.toString()
            val message = "LTO VEHICLE" + " " + platenumber

            smsManager!!.sendTextMessage(number, null, message, null, null)

            Toast.makeText(context, "The Plate number has been processed. Please check your inbox.", Toast.LENGTH_LONG).show()

            val intent = Intent(context, NavigationActivity::class.java)
            startActivity(intent)
        }
    }
}
