package batstate.commutersalertbutton.data;

public class ContactData {
    public String name;
    public String contactNo;

    public ContactData() {
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof ContactData) {
            ContactData other = (ContactData) o;

            return name.equals(other.name) &&
                    contactNo.equals(other.contactNo);
        }
        return false;
    }

    @Override
    public int hashCode() {
        int hash = 31;
        hash = hash * 37 * name.hashCode();
        hash = hash * 37 * contactNo.hashCode();
        return hash;
    }
}