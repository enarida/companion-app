package batstate.commutersalertbutton.adapter

import android.content.Context
import android.content.res.Resources
import android.support.v7.app.AlertDialog
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import batstate.commutersalertbutton.R
import batstate.commutersalertbutton.data.ContactData
import batstate.commutersalertbutton.util.OnStatusChangeListener
import batstate.commutersalertbutton.util.SettingsUtil
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.util.ArrayList

class ContactAdapter(private val mContext: Context, private val mSaveToSettings: Boolean) : BaseAdapter(), AdapterView.OnItemClickListener {
    private val mInflater: LayoutInflater = mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
    private val mResources: Resources = mContext.resources
    private val mData: ArrayList<ContactData> = ArrayList()
    private var mPriorityContact: Int = 0

    // Event Listeners
    private var mOnStatusChangeListener: OnStatusChangeListener? = null

    init {
        mPriorityContact = 0
    }

//   Add contacts
    fun addItem(contact: ContactData) {
        mData.add(contact)
        notifyDataSetChanged()

        if (mSaveToSettings)
            save()

        if (mOnStatusChangeListener != null) {
            val status = if (mData.size > 0)
                OnStatusChangeListener.Status.NO_ERROR
            else
                OnStatusChangeListener.Status.HAS_ERROR
            mOnStatusChangeListener!!.onStatusChange(status)
        }
    }

    operator fun contains(contact: ContactData): Boolean {
        return mData.contains(contact)
    }

    fun setPriorityContact(index: Int) {
        mPriorityContact = index
        SettingsUtil.setInt(mContext, SettingsUtil.KEY_PRIORITY_CONTACT_INDEX, index)
        notifyDataSetInvalidated()
    }

    override fun getItem(i: Int): ContactData {
        return mData[i]
    }

    override fun getItemId(i: Int): Long {
        return i.toLong()
    }

    override fun getCount(): Int {
        return mData.size
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        var convertView = convertView
        val holder: ViewHolder
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.layout_contacts, parent, false)
            holder = ViewHolder()
            holder.text1 = convertView.findViewById<TextView>(R.id.text1) as TextView
            holder.text2 = convertView.findViewById<TextView>(R.id.text2) as TextView
            holder.icon = convertView.findViewById<ImageView>(R.id.icon) as ImageView

            convertView.tag = holder
        } else
            holder = convertView.tag as ViewHolder

        val contact = mData[position]
        val iconRes = if (position == mPriorityContact) R.drawable.ic_check_circle_black_24dp else 0
        holder.text1!!.text = contact.name
        holder.text2!!.text = contact.contactNo
        holder.icon!!.setImageResource(iconRes)

        return convertView!!
    }

    private class ViewHolder {
        internal var text1: TextView? = null
        internal var text2: TextView? = null
        internal var icon: ImageView? = null
    }

    override fun onItemClick(parentView: AdapterView<*>, childView: View, position: Int, id: Long) {
        val contact = mData[position]
        val title = contact.name + " (" + contact.contactNo + ")"

        val dialogAdapter = ArrayAdapter<String>(
                mContext,
                android.R.layout.select_dialog_item)

        // Load options from resource
        for (opt in mResources.getStringArray(R.array.contact_options)) {
            dialogAdapter.add(opt)

            if (count == 1)
                break
        }

        AlertDialog.Builder(mContext)
                .setTitle(title)
                .setAdapter(dialogAdapter, { dialogInterface, i ->
                    if (i == 0) {
                        setPriorityContact(position)
                        notifyDataSetInvalidated()
                    } else {
                        mData.removeAt(position)

                        if (mPriorityContact >= position)
                            mPriorityContact--
                        if (mData.size == 0)
                            mPriorityContact = -1

                        notifyDataSetChanged()
                        save()

                        if (mOnStatusChangeListener != null) {
                            val status = if (mData.size > 0)
                                OnStatusChangeListener.Status.NO_ERROR
                            else
                                OnStatusChangeListener.Status.HAS_ERROR
                            mOnStatusChangeListener!!.onStatusChange(status)
                        }
                    }
                }).show()
    }

    fun setOnStatusChangeListener(listener: OnStatusChangeListener?) {
        mOnStatusChangeListener = listener
    }

    private fun save() {
        // Serialize Contact List
        val contactListJson = JSONArray()
        try {
            for (i in 0 until count) {
                val contactItem = getItem(i)
                val contactObjJson = JSONObject()
                contactObjJson.put("name", contactItem.name)
                contactObjJson.put("contactNo", contactItem.contactNo)
                contactListJson.put(contactObjJson)
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        // Save settings
        SettingsUtil.setString(mContext, SettingsUtil.KEY_CONTACTS, contactListJson.toString())
    }
}
