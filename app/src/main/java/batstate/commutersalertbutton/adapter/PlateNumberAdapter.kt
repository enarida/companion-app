package batstate.commutersalertbutton.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.content.DialogInterface
import android.support.v7.app.AlertDialog
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.BaseAdapter
import android.widget.EditText
import android.widget.TextView
import batstate.commutersalertbutton.R
import batstate.commutersalertbutton.util.OnStatusChangeListener
import batstate.commutersalertbutton.util.SettingsUtil
import java.util.ArrayList
import java.util.HashMap

class PlateNumberAdapter(private val mContext: Context) : BaseAdapter(), AdapterView.OnItemClickListener {

    private val mInflater: LayoutInflater = mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
    private val mData: ArrayList<HashMap<String, Any>> = ArrayList()

    // Event Listeners
    private var mOnStatusChangeListener: OnStatusChangeListener? = null

    fun addItem(settingsKey: String, title: String, value: String, filter: Int) {
        val map = HashMap<String, Any>(4)
        map["settingsKey"] = settingsKey
        map["title"] = title
        map["value"] = value
        map["filter"] = filter
        mData.add(map)
        notifyDataSetChanged()
    }

    override fun getItem(i: Int): HashMap<String, Any> {
        return mData[i]
    }

    override fun getItemId(i: Int): Long {
        return i.toLong()
    }

    override fun getCount(): Int {
        return mData.size
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        var convertView = convertView
        val data = mData[position]
        val holder: ViewHolder
        if (convertView == null) {
            convertView = mInflater.inflate(android.R.layout.simple_list_item_2, parent, false)
            holder = ViewHolder()
            holder.text1 = convertView!!.findViewById<View>(android.R.id.text1) as TextView
            holder.text2 = convertView.findViewById<View>(android.R.id.text2) as TextView
            convertView.tag = holder
        } else
            holder = convertView.tag as ViewHolder

        holder.text1!!.text = data["title"].toString()
        var value = data["value"].toString()
        if (value.isEmpty())
            value = "Tap to select"

        holder.text2!!.text = value

        return convertView
    }

    override fun onItemClick(parentView: AdapterView<*>, childView: View, position: Int, id: Long) {
        @SuppressLint("InflateParams") val view = mInflater.inflate(R.layout.dialog_edit_profile, null)
        val editText = view.findViewById<EditText>(R.id.input_text1) as EditText

        val data = mData[position]
        val settingsKey = data["settingsKey"].toString()
        val title = data["title"].toString()
        val value = data["value"].toString()
        val filter = data["filter"] as Int

        if (filter == FILTER_PLATE_NUMBER)
            editText.addTextChangedListener(PlateNumberFilter(editText))

        // Set edit text's initial value
        if (!value.isEmpty()) {
            editText.setText(value)
            editText.setSelection(editText.text.length)
        }

        val dialog = AlertDialog.Builder(mContext)
                .setTitle(title)
                .setView(view)
                .setNegativeButton(mContext.getString(R.string.discard_button), { dialog, _ ->
                    dialog.dismiss()

                    if (mOnStatusChangeListener != null) {
                        val status = if (hasErrors())
                            OnStatusChangeListener.Status.HAS_ERROR
                        else
                            OnStatusChangeListener.Status.NO_ERROR
                        mOnStatusChangeListener!!.onStatusChange(status)
                    }
                })
                .setPositiveButton(mContext.getString(R.string.save_button), { _, _ -> })
                .create()

        dialog.setCanceledOnTouchOutside(false)
        dialog.setOnShowListener({
            dialog.getButton(AlertDialog.BUTTON_POSITIVE)
                    .setOnClickListener({
                        if (editText.error == null && editText.text.isNotEmpty()) {
                            val newVal = editText.text.toString().trim { it <= ' ' }
                            data["value"] = newVal
                            SettingsUtil.setString(mContext, settingsKey, newVal)
                            notifyDataSetChanged()
                            dialog.dismiss()

                            if (mOnStatusChangeListener != null) {
                                val status = if (hasErrors())
                                    OnStatusChangeListener.Status.HAS_ERROR
                                else
                                    OnStatusChangeListener.Status.NO_ERROR
                                mOnStatusChangeListener!!.onStatusChange(status)
                            }
                        } else {
                            editText.error = mContext.getString(R.string.error_field_blank)
                        }
                    })
        })
        dialog.show()
    }

    private class ViewHolder {
        internal var text1: TextView? = null
        internal var text2: TextView? = null
    }

    fun hasErrors(): Boolean {
        for (data in mData) {
            if (data["value"].toString().isEmpty())
                return true
        }
        return false
    }

    private inner class PlateNumberFilter internal constructor(private val mEditText: EditText) : TextWatcher {

        override fun afterTextChanged(s: Editable) {}

        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

        override fun onTextChanged(value: CharSequence, start: Int, before: Int, count: Int) {
            if (value.isEmpty())
                mEditText.error = "This field cannot be blank"
                mEditText.error = null
        }
    }

    companion object {

        const val FILTER_PLATE_NUMBER = 1
    }
}