package batstate.commutersalertbutton

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.app.Fragment
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.view.View
import android.view.inputmethod.InputMethodManager
import kotlinx.android.synthetic.main.activity_navigation.*
import kotlinx.android.synthetic.main.app_bar_navigation.*


class NavigationActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

//   Fragments
    private val homeFragment = HomeFragment()
    private val mmdaFragment = MMDAFragment()
    private val plateNumberDetectorFragment = PlateNumberDetectorFragment()
    private val loanFragment = LoanFragment()
    private val aboutFragment = AboutFragment()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_navigation)
        setSupportActionBar(toolbar)
        setupDrawer()
        setFragment(homeFragment)
    }

//   Navigation Drawer
    private fun setupDrawer() {

        val actionBarDrawerToggle = object : ActionBarDrawerToggle(this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {

            override fun onDrawerOpened(drawerView: View) {
                super.onDrawerOpened(drawerView)
                val inputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                inputMethodManager.hideSoftInputFromWindow(currentFocus!!.windowToken, 0)
            }

            override fun onDrawerSlide(drawerView: View, slideOffset: Float) {
                super.onDrawerSlide(drawerView, slideOffset)
                val inputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                inputMethodManager.hideSoftInputFromWindow(currentFocus!!.windowToken, 0)
            }
        }

        drawer_layout.addDrawerListener(actionBarDrawerToggle)
        actionBarDrawerToggle.syncState()
        nav_view.setNavigationItemSelectedListener(this)
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

//    When a menu is selected on navigation drawer
    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        when (item.itemId) {
            R.id.nav_home -> {
                setFragment(homeFragment)
                title = "Commuters Alert Button"
            }
            R.id.nav_mmda -> {
                setFragment(mmdaFragment)
                title = "MMDA May Huli Ba?"
            }
            R.id.nav_plateNumberDetector -> {
                setFragment(plateNumberDetectorFragment)
                title = "LTO Plate Number Detector"
            }
            R.id.nav_loanForLoad -> {
                setFragment(loanFragment)
                title = "Loan for load"
            }
            R.id.nav_settings -> {
                val intent = Intent(this,SettingsActivity::class.java)
                startActivity(intent)
            }
            R.id.nav_about -> {
                setFragment(aboutFragment)
                title = "About"
            }
        }

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

//   Setup Fragment
    private fun setFragment(fragment: Fragment) {
        supportFragmentManager.beginTransaction()
                .replace(R.id.fragmentPlaceholder, fragment)
                .commit()
    }
}
