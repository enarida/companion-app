package batstate.commutersalertbutton

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_loan.*

class LoanFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        return inflater!!.inflate(R.layout.fragment_loan, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupButtons()
    }

    private fun setupButtons() {

        val encodedhash = Uri.encode("#")
        val globeNumber = "*143$encodedhash"
        val tntNumber = "*7572"
        val smartNumber = "*767"

        btn_globe.setOnClickListener {
            val callIntent = Intent(Intent.ACTION_CALL, Uri.parse("tel:$globeNumber"))
            startActivity(callIntent)
        }
        btn_talkntext.setOnClickListener {
            val callIntent = Intent(Intent.ACTION_CALL, Uri.parse("tel:$tntNumber"))
            startActivity(callIntent)
        }
        btn_smart.setOnClickListener {
            val callIntent = Intent(Intent.ACTION_CALL, Uri.parse("tel:$smartNumber"))
            startActivity(callIntent)
        }
    }
}
