package batstate.commutersalertbutton

import android.content.Intent
import android.location.Location
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import batstate.commutersalertbutton.util.LocationManager
import batstate.commutersalertbutton.util.PublishService
import batstate.commutersalertbutton.util.SettingsUtil
import kotlinx.android.synthetic.main.fragment_home.*

class HomeFragment : Fragment() {

    private var locationManager: LocationManager? = null

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupEmergencyButton()

//       Location Listener (Updates text when changes accuracy)
        locationManager = LocationManager(activity)
        locationManager!!.setLocationListener({
            txt_accuracy.text = String.format("Location Accuracy is %sm", locationManager!!.currentLocation.accuracy)
        })

    }

    override fun onResume() {
        super.onResume()
        locationManager?.start()
        if (locationManager!!.currentLocation != null) {
            txt_accuracy.text = String.format("Location Accuracy is %sm", locationManager!!.currentLocation.accuracy)
        } else {
            txt_accuracy.setText(R.string.waiting_for_location)
        }
    }

    override fun onPause() {
        super.onPause()
        locationManager!!.stop()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        super.onActivityResult(requestCode, resultCode, data)
        locationManager?.onActivityResult(requestCode, resultCode)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        return inflater!!.inflate(R.layout.fragment_home, container, false)
    }



    private fun setupEmergencyButton() {
        emergencyButton.setOnClickListener {

            // Send SMS to Contacts
            val publishIntent = Intent(context, PublishService::class.java)
            val bundle = Bundle()
            publishIntent.putExtras(bundle)
            context.startService(publishIntent)

            // Call Priority Contact
            val priorityContact = SettingsUtil.getPriorityContact(context)
            if (priorityContact != null) {
                val callIntent = Intent(Intent.ACTION_CALL, Uri.parse("tel:" + priorityContact.contactNo))
                startActivity(callIntent)
            } else {
                Toast.makeText(context, "No priority contact to call", Toast.LENGTH_LONG).show()
            }

            Log.d("Button", "Tapped")
        }
    }
}